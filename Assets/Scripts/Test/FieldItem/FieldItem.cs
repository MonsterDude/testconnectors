﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;

public class FieldItem : Fieldable
{
    [Inject] ColorManager _colorManager; 
    [SerializeField] MeshRenderer rend;

    private void Start()
    {
        SetState(ObjectState.Default);
    }

    public override void AddConnection(Connection_LR conn)
    {
        connections.Add(conn);
    }

    public override void RemoveConnection(Connection_LR conn)
    {
        connections.Remove(conn);
    }

    public override void SetState(ObjectState state)
    {
        rend.material.color = _colorManager.StateColors[state]; 
    } 

    public override void Move(Vector3 position)
    {
        transform.position = position;
        for (int i = 0; i < connections.Count; i++)
        {
            connections[i].UpdatePositions();
        }
    }
}

public abstract class Fieldable:MonoBehaviour
{
    [SerializeField] protected List<Connection_LR> connections;
    public virtual IReadOnlyList<Connection_LR> Connections => connections;
    public enum ObjectState { Default, Selected, Deselected}
    protected ObjectState currentState;

    public abstract void AddConnection(Connection_LR conn);
    public abstract void RemoveConnection(Connection_LR conn); 
    public abstract void SetState(ObjectState state);
    public abstract void Move(Vector3 position);

    public virtual bool ConnectsTo(Fieldable other)
    {
        Connection con = this.connections.Where((x) => x.CheckPairing(this, other)).FirstOrDefault();
        if (con == null)
        {
            con = other.connections.Where((x) => x.CheckPairing(this, other)).FirstOrDefault();
            return con != null;
        }
        return true;
    }
}


