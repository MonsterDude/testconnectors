﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[System.Serializable] //So we can change it through editor!
public class RadialFactory : IfieldableFactory
{
    [Inject] DiContainer _container;
    [Inject] FieldManager _fieldManager; 

    [SerializeField] float _radius;
    [SerializeField] Vector3 _center;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="radius">inclusive</param>
    /// <param name="center"></param>
    public RadialFactory(float radius, Vector3 center)
    {
        _radius = radius;
        _center = center;
    } 

    public T[] Create<T>(GameObject prefab, int count) where T : Fieldable
    {
        T[] output = new T[count];
        for (int i = 0; i < count; i++)
        {
            //предполагаем угол, предполагаем радиус
            float angle = Random.Range(0, 359);
            float radius = Random.Range(0, _radius);
            Vector3 position = new Vector3(_center.x + radius * Mathf.Cos(angle), _center.y, _center.z + radius * Mathf.Sin(angle));
            //TODO: integrate pool system
            GameObject obj = _container.InstantiatePrefab(prefab, position, Quaternion.identity, _fieldManager.FieldableRoot);
                //GameObject.Instantiate(prefab, position, Quaternion.identity); 
            output[i] = obj.GetComponent<T>();
            if (output[i] == null)
                output[i] = obj.GetComponentInChildren<T>();
            if (output[i] == null)
                Debug.LogError($"Eroor while building object {prefab} of type {typeof(T)}");
        }
        return output;
    }
}
 
public interface IfieldableFactory
{
    T[] Create<T>(GameObject prefab, int count) where T : Fieldable;
     
}
