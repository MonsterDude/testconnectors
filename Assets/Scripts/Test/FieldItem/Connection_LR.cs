﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connection_LR : MonoBehaviour, Connection
{

    [SerializeField] LineRenderer _lineRenderer;
    Fieldable A, B;
      
    public void SetConnections(Fieldable A, Fieldable B)
    {
        this.A = A;
        this.B = B;
        A.AddConnection(this);
        B.AddConnection(this);
        _lineRenderer.enabled = true;
        UpdatePositions(); 
    }

    private void OnDestroy()
    {
        RemoveSelf(A);
        RemoveSelf(B);

        void RemoveSelf(Fieldable f)
        {
            if (f != null)
                f.RemoveConnection(this);
        }
    }

    public void UpdatePositions()
    {
        _lineRenderer.SetPosition(0, A.transform.position);
        _lineRenderer.SetPosition(1, B.transform.position);
    }

    public bool CheckPairing(Fieldable A, Fieldable B)
    {
        return (A == this.A && B == this.B || A == this.B && B == this.A);
    }
}

public interface Connection 
{ 

    void SetConnections(Fieldable A, Fieldable B);
    void UpdatePositions();
    bool CheckPairing(Fieldable A, Fieldable B);

}
