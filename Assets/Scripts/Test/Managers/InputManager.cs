﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class InputManager : MonoBehaviour
{
    [Inject] EventSystem _eventSystem;
    [SerializeField] GraphicRaycaster graphicRaycaster;

    public delegate void Notify(GameObject obj); 
    public event Notify onhit;

    public bool overUI { get; private set; }

    public GameObject Raycast()
    {
        overUI = false;
        GameObject output = null;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        PointerEventData _PEData = new PointerEventData(_eventSystem);
        _PEData.position = Input.mousePosition;
        graphicRaycaster.Raycast(_PEData, raycastResults);
        if (raycastResults.Count > 0)
        {
            bool anyUIcatcher = false;
            //if (raycastResults != null)
            //{
            //    for (int i = 0; i < raycastResults.Count; i++)
            //    {
            //        if (raycastResults[i].gameObject.layer != (int)LayerManager.Layers.UI_Ignore)
            //        {
                        anyUIcatcher = true;

            //            break;
            //        }
            //    }
            //}
            overUI = anyUIcatcher;
            output = raycastResults[0].gameObject;
        }
        else
        {
            Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //if (Physics.Raycast(new Ray(camPos, camPos + Camera.main.transform.rotation * Vector3.forward), out hit, float.MaxValue/*, LayerManager.Field*/))
            //{ 
            //    transform.position = hit.point;
            //}
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),out hit, 999/*, 1 << 32*/))
            {
                output = hit.transform.gameObject;
            }
        } 
        return output;
    }
}
