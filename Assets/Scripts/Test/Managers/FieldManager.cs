﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static Fieldable;
using Zenject;

public class FieldManager : MonoBehaviour
{
    [Inject] PrefabManager _prefabManager;
    [Inject] DiContainer _container; 


    [SerializeField] List<Fieldable> _fieldItems;
    [SerializeField] Transform _fieldableRoot;
    public Transform FieldableRoot => _fieldableRoot;
    [SerializeField] Transform _connectorRoot;
    public Transform ConnectorRoot => _connectorRoot;

    public void Recreate(IfieldableFactory factory, int amount)
    {
        DeleteAll();
        _fieldItems = factory.Create<FieldItem>(_prefabManager.FieldItemPF, amount).ToList<Fieldable>(); 
    }

    public void DeleteAll()
    {
        for (int i = _fieldItems.Count-1; i >-1; i--)
        {
            Destroy(_fieldItems[i]);
        }
    }

    /// <summary>
    /// Set state of every object
    /// </summary>
    /// <param name="state"></param>
    public void SetStates(ObjectState state)
    {
        for (int i = 0; i < _fieldItems.Count; i++)
        {
            _fieldItems[i].SetState(state);
        }
    }

    public void SetStates(ObjectState state, List<Fieldable> exceptions, ObjectState stateException)
    { 
        for (int i = 0; i < exceptions.Count; i++)
        {
            exceptions[i].SetState(stateException); 
        } 
        for (int i = 0; i < _fieldItems.Count; i++)
        {
            if (!exceptions.Contains(_fieldItems[i]))
                _fieldItems[i].SetState(state);
        }
    }

    public void CreateConnection(Fieldable A, Fieldable B)
    {
        if (!CheckValidity(A,B))
            return;
        Connection c = _container.InstantiatePrefab(_prefabManager.ConnectionPF, Vector3.zero, Quaternion.identity, ConnectorRoot).GetComponent<Connection>();
        c.SetConnections(A, B);
    }

    public bool CheckValidity(Fieldable A, Fieldable B)
    {
        bool output = true;
        if (A == B)
            output = false;
        if (A.ConnectsTo(B))
            output = false;
        return output;
    }

}
