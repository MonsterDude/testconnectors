﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class MainInstaller : MonoInstaller
{
    [SerializeField] Main _main;
    [SerializeField] InputManager _inputManager;
    [SerializeField] FieldManager _fieldManager;
    [SerializeField] ColorManager _colorManager;
    [SerializeField] PrefabManager _prefabManager;
    [SerializeField] LineRenderer _lineRenderer;

    [SerializeField] EventSystem _eventSystem;

    public override void InstallBindings()
    {
        Container.Bind<InputManager>().FromInstance(_inputManager).AsSingle().NonLazy();
        Container.Bind<Main>().FromInstance(_main).AsSingle();
        Container.Bind<FieldManager>().FromInstance(_fieldManager).AsSingle();
        Container.Bind<ColorManager>().FromInstance(_colorManager).AsSingle();
        Container.Bind<PrefabManager>().FromInstance(_prefabManager).AsSingle();
        Container.Bind<LineRenderer>().FromInstance(_lineRenderer).AsSingle();

        Container.Bind<EventSystem>().FromInstance(_eventSystem).AsSingle();
    } 
}
