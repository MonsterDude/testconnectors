﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    [SerializeField] GameObject _fieldItemPF;
    public GameObject FieldItemPF => _fieldItemPF;
    [SerializeField] GameObject _connectionPF;
    public GameObject ConnectionPF => _connectionPF;
}
