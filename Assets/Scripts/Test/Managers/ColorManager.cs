﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Fieldable;

public class ColorManager : MonoBehaviour
{
    [SerializeField] List<COlorKeyValuePair> _colorstatelist = new List<COlorKeyValuePair>(System.Enum.GetNames(typeof(ObjectState)).Length);
    public IReadOnlyDictionary<ObjectState, Color> StateColors { get; private set; }

    // Start is called before the first frame update
    void Awake()
    {
        var temp = new Dictionary<ObjectState, Color>();
        for (int i = 0; i < _colorstatelist.Count; i++)
        {
            temp.Add(_colorstatelist[i].key, _colorstatelist[i].value);
        }
        StateColors = temp;
    } 
}

//костыли-костылики
[System.Serializable]
public class COlorKeyValuePair
{
    public ObjectState key;
    public Color value;
}