﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static Fieldable;

public class HoldSelection : ConcreteSelectionStrategy
{
    [Inject] LineRenderer _lineRendrer;

    public HoldSelection()
    {
        movementMouseKey = 1;
    }

    public override void ProcessUpdate()
    {
        if (current != null)
            return;
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            GameObject go = _inputManager.Raycast();
            if (go != null)
                currentObject = go.GetComponent<Fieldable>();
            if (currentObject != null)
            {
                if (Input.GetMouseButtonDown(0))
                    current = _main.StartCoroutine(WaitForInput(() => Input.GetMouseButtonDown(0), null, () => current = _main.StartCoroutine(Connect())));
                else if (Input.GetMouseButtonDown(1))
                    current = _main.StartCoroutine(WaitForInput(() => Input.GetMouseButton(1), null, () => current = _main.StartCoroutine(Move())));
            } 
        }
    }

    protected override IEnumerator Connect()
    {
        Plane plane = new Plane(Vector3.down, currentObject.transform.position.y);
        _fieldManager.SetStates(ObjectState.Deselected, new List<Fieldable>() { currentObject }, ObjectState.Selected);
        _lineRendrer.SetPosition(0, currentObject.transform.position);
        _lineRendrer.enabled = true;
        Fieldable f = null;
        while (Input.GetMouseButton(0))
        {
            float distance;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out distance))
            {
                _lineRendrer.SetPosition(1, ray.GetPoint(distance));
            }
            List<Fieldable> selected = new List<Fieldable>() { currentObject };
            GameObject go = _inputManager.Raycast();
            if (go != null)
            { 
                f = go.GetComponent<Fieldable>();
                if (f != null)
                    selected.Add(f);
            }
            _fieldManager.SetStates(ObjectState.Deselected, selected, ObjectState.Selected);
            yield return null;
        }
        if (f != null)
        {
            _fieldManager.CreateConnection(currentObject, f); 
        }
        _lineRendrer.enabled = false;
        currentObject = null;
        _fieldManager.SetStates(ObjectState.Default);
        current = null; 
    }
}
 