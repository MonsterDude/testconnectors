﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static Fieldable;

public class ClickSelection : ConcreteSelectionStrategy
{
    public ClickSelection()
    {
        movementMouseKey = 0;
    }

    public override void ProcessUpdate()
    { 
        if (current != null)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            GameObject go = _inputManager.Raycast();
            if(go!=null)
                currentObject = go.GetComponent<Fieldable>();
            if(currentObject!=null)
                current = _main.StartCoroutine(WaitForInput(()=> Input.GetMouseButton(0), ()=>current = _main.StartCoroutine(Connect()), ()=> current = _main.StartCoroutine(Move()))); 
        }
    } 

    override protected IEnumerator Connect()
    {
        _fieldManager.SetStates(ObjectState.Deselected, new List<Fieldable>() { currentObject }, ObjectState.Selected);
        bool exit;
        while (true)
        {
            exit = false;
            if (Input.GetKey(KeyCode.Escape))
                exit = true;
            else if (Input.GetMouseButtonDown(0))
            {
                GameObject second = _inputManager.Raycast();
                if (second != null || second!=currentObject) { 
                    Fieldable f = second.GetComponent<Fieldable>();
                    if (f != null)
                    {
                        _fieldManager.CreateConnection(currentObject, f);
                    } 
                }
                exit = true;
            }
            if (exit)
            {
                currentObject = null;
                _fieldManager.SetStates(ObjectState.Default);
                break;
            }
            yield return null; 
        }
        current = null;
    } 
}
