﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Action = System.Action;

public interface IselectionStrategy
{
    void ProcessUpdate(); 
     
}

public abstract class ConcreteSelectionStrategy : IselectionStrategy
{
    [Inject] protected Main _main;
    [Inject] protected InputManager _inputManager;
    [Inject] protected FieldManager _fieldManager;

    protected int movementMouseKey;

    protected Coroutine current;
    protected Fieldable currentObject;
    protected abstract IEnumerator Connect();

    protected IEnumerator WaitForInput(System.Func<bool> resolver, Action click, Action hold)
    {
        yield return new WaitForSeconds(_main.settings_clickReactionTime);
        if ((bool)resolver.DynamicInvoke())//holding
            current = _main.StartCoroutine(Move());
        else
            current = _main.StartCoroutine(Connect());
    }

    protected IEnumerator Move()
    {
        Plane plane = new Plane(Vector3.down, currentObject.transform.position.y);
        while (Input.GetMouseButton(movementMouseKey))
        {

            float distance;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out distance))
            {
                currentObject.Move(ray.GetPoint(distance));
            }
            yield return null;
        }
        current = null;
    } 
    public virtual void ProcessUpdate()
    {
        if (current != null)
            return;
    }
}
