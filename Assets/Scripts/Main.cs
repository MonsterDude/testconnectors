﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Main : MonoBehaviour
{
    public float Radius = 10; 
    public float height = -4; //field height
    public float settings_clickReactionTime; //TODO: ceate settings class

    [Inject] InputManager _inputManager;
    [Inject] FieldManager _fieldManager;
    [Inject] DiContainer _container;
    [Inject] ColorManager _colorManager;

    //custom
    [SerializeField /*, SerializeReference sadly, doesnt work in 2019.2*/] IfieldableFactory _currentFactory;
    [SerializeField /*, SerializeReference sadly, doesnt work in 2019.2*/] IselectionStrategy _currentSelection; 

    void Start()
    {
        _currentFactory = new RadialFactory(Radius,Vector3.up * height); 
        _container.Inject(_currentFactory);

        _fieldManager.Recreate(_currentFactory,  10);

        _currentSelection = new HoldSelection();
        _container.Inject(_currentSelection); 
    }

    private void Update()
    {
        _currentSelection?.ProcessUpdate();
    }

    public void ChangeSelectionMode()
    {
        IselectionStrategy newSelection;
        if (_currentSelection.GetType() == typeof(ClickSelection))
            newSelection = new HoldSelection();
        else
            newSelection = new ClickSelection();
        _currentSelection = newSelection;
        _container.Inject(newSelection);
        Debug.LogError($"New selection: {newSelection.GetType()}"); 
    }
}

